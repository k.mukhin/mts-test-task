from apscheduler.schedulers.background import BackgroundScheduler
import requests
import logging
import flask
import yaml
import sys

logging.basicConfig(format='%(levelname)-8s [%(asctime)s] %(message)s',
                    level=logging.DEBUG,
                    filename='service.log')


# class for read config and convert "yml to dict"
class ConfigReader():

    def __init__(self, config_path):
        self.config_path = config_path

    def parse_config(self):
        with open(self.config_path, 'r') as stream:
            try:
                self.config = yaml.load(stream)
            except yaml.YAMLError as exc:
                logging.debug(f'Error in parse config {exc}')


# Get first arg from CLI
c = ConfigReader(str(sys.argv[1]))
c.parse_config()


# Update config
def update_config():
    c.parse_config()
    logging.debug(f'Config update')


###########################
# Processing JSON
def parse_json(data):
    tasks = {}
    for p in data:
        for j in c.config['jobs']:
            if p['project'] in j['projects']:
                job = j['job_name']
                tasks[job] = p['params']

    return tasks


# Build URL for Jenkins
def build_uri(job_name, params):
    host = c.config['jenkins_url'].split('//')[1]
    user = c.config['jenkins_user']
    password = c.config['jenkins_pass']
    uri = list()
    uri.append(f'http://{user}:{password}@{host}')
    uri.append(f'/job/{job_name}/buildWithParameters?')
    for k in params:
        uri.append(f'{k}={params[k]}&')
    uri = "".join(uri)
    logging.debug(f'Build URI  {uri}')
    return uri[:-1]


# Send POST request
def send_jenkins(tasks):
    for t in tasks:
        job_name = t
        params = tasks[t]
        uri = build_uri(job_name, params)
        requests.post(uri)
        logging.debug(f'Send requiest to Jenkins for {job_name} and parametrs {params}')


###########################
# Web app for catch POST request (API)
app = flask.Flask(__name__)


@app.route('/', methods=['POST'])
def post():
    json = flask.request.get_json()
    logging.debug(f'Get JSON: {json}')
    tasks = parse_json(json)
    send_jenkins(tasks)
    return flask.Response(status='200')


if __name__ == '__main__':
    sched = BackgroundScheduler(daemon=True)
    sched.add_job(update_config, 'interval', seconds=60)
    sched.start()
    app.debug = True
    app.run(host='0.0.0.0', port=80)
