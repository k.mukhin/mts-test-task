FROM centos/systemd

RUN yum -y install epel-release openssh-server sudo openssl
RUN yum -y install python36 python36-devel python36-pip

RUN mkdir -p /var/run/sshd ; chmod -rx /var/run/sshd
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd
RUN sed -ri 's/#PermitRootLogin yes/PermitRootLogin yes/g' /etc/ssh/sshd_config
RUN sed -ri 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config
RUN sed -ri 's/#UsePAM no/UsePAM no/g' /etc/ssh/sshd_config
RUN \
    ssh-keygen -q -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa && \
    ssh-keygen -q -f /etc/ssh/ssh_host_ecdsa_key -N '' -t ecdsa && \
    ssh-keygen -q -f /etc/ssh/ssh_host_ed25519_key -N '' -t ed25519 && \
    mkdir -p /root/.ssh

ADD ./requirements.txt /opt/service/requirements.txt
ADD ./conf.yml /opt/service/conf.yml
ADD ./service.py /opt/service/service.py
ADD ./key.pub /root/.ssh/authorized_keys
RUN chown root /root/.ssh/authorized_keys; chmod 600 /root/.ssh/authorized_keys

WORKDIR /opt/service/
RUN pip3.6 install -r requirements.txt

COPY python.service /etc/systemd/system/
RUN chmod 664 /etc/systemd/system/python.service
RUN systemctl enable python.service
RUN systemctl enable sshd.service

EXPOSE 22 80
CMD ["/usr/sbin/init"]
